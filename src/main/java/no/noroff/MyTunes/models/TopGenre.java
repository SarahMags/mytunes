package no.noroff.MyTunes.models;

public class TopGenre { //TopGenre pojo

    private String genreName;
    private int maxGenreNumber;

    public TopGenre(){

    }
//Top genre getters
    public String getGenreName() {
        return genreName;
    }

    public int getMaxGenreNumber() {
        return maxGenreNumber;
    }


    public TopGenre(String genreName, int maxGenreNumber) { //TopGenre constructor contains genre name and number that corresponds to a customers most popular genre
        this.genreName = genreName;
        this.maxGenreNumber = maxGenreNumber;
    }
}


