package no.noroff.MyTunes.models;
//model for Customer with the information regarding number of customers in a country
public class Country {
    private String country;
    private int numberOfCustomers;

    public Country() {
    }

    //constructor with the wanted information
    public Country(String country, int numberOfCustomers){
        this.country = country;
        this.numberOfCustomers = numberOfCustomers;
    }

    //getters and setter to access the variables outside the class
    public int getNumberOfCustomers() {

        return numberOfCustomers;
    }

    public void setNumberOfCustomers(int numberOfCustomers) {

        this.numberOfCustomers = numberOfCustomers;
    }

    public String getCountry() {

        return country;
    }

    public void setCountry(String country) {

        this.country = country;
    }


}
