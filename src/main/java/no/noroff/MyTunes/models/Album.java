package no.noroff.MyTunes.models;
//model for Album with the information regarding albums
public class Album {

    private String albumName;

    public Album() {
    }
    //Access the variable outside the class
    public String getAlbumName() {

        return albumName;
    }
    //constructor with the album information
    public Album(String albumName) {

        this.albumName = albumName;
    }
}
