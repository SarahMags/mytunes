package no.noroff.MyTunes.models;
//model for Artist with the information regarding artists
public class Artist {

    private String artistName;

    public Artist() {
    }
    //Access the variable outside the class
    public String getArtistName() {

        return artistName;
    }
    //constructor
    public Artist(String artistName) {

        this.artistName = artistName;
    }
}
