package no.noroff.MyTunes.models;

public class Song { //Song pojo

    private String songName;

    public Song(){

    }
    //Song getter
    public String getSongName() {
        return songName;
    }

    public Song(String songName) {
        this.songName = songName;
    } //Song constructor contains track name
}
