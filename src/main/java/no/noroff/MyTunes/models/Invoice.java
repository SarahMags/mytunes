package no.noroff.MyTunes.models;

public class Invoice { //Invoice pojo

    private int customerId;
    private String firstName;
    private String lastName;
    private double totalSpent;

    public Invoice(){

    }
//Invoice getters
    public int getCustomerId() {
        return customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public double getTotalSpent() {
        return totalSpent;
    }

    public Invoice (int customerId, String firstName, String lastName, double totalSpent){ //Invoice constructor contains id, first name, last name and total spent
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.totalSpent = totalSpent;
    }


}
