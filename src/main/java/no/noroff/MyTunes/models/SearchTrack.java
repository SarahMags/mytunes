package no.noroff.MyTunes.models;

public class SearchTrack { //SearchTrack pojo
    private String songName;
    private String artistName;
    private String albumName;
    private String genreName;

    public SearchTrack() {
    }

//SearchTrack getters
    public String getSongName() {
        return songName;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getAlbumName() {
        return albumName;
    }


    public String getGenreName() {
        return genreName;
    }

    public SearchTrack(String songName, String artistName, String albumName, String genreName) { //SearchTrack constructor contains names for track, artist, album and genre
        this.songName = songName;
        this.artistName = artistName;
        this.albumName = albumName;
        this.genreName = genreName;

    }
}
