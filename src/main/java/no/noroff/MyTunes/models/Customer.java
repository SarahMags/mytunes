package no.noroff.MyTunes.models;

import java.util.ArrayList;
//model for Customer with the information regarding a customer.
public class Customer {
    private int customerId;
    private String firstName;
    private String lastName;
    private String country;
    private String postalCode;
    private String phoneNr;
    private String email;

    public Customer() {
    }
    //constructor with the customer information
    public Customer(int customerId, String firstName, String lastName,
                    String country, String postalCode, String phoneNr, String email){
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.postalCode = postalCode;
        this.phoneNr = phoneNr;
        this.email = email;
    }
    //getters and setter to access the variables outside the class
    public int getCustomerId() {

        return customerId;
    }

    public void setCustomerId(int customerId) {

        this.customerId = customerId;
    }

    public String getFirstName() {

        return firstName;
    }

    public void setFirstName(String firstName) {

        this.firstName = firstName;
    }

    public String getLastName() {

        return lastName;
    }

    public void setLastName(String lastName) {

        this.lastName = lastName;
    }

    public String getCountry() {

        return country;
    }

    public void setCountry(String country) {

        this.country = country;
    }

    public String getPostalCode() {

        return postalCode;
    }

    public void setPostalCode(String postalCode) {

        this.postalCode = postalCode;
    }

    public String getPhoneNr() {

        return phoneNr;
    }

    public void setPhoneNr(String phoneNr) {

        this.phoneNr = phoneNr;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

}
