package no.noroff.MyTunes.models;

public class Genre { //Genre pojo

    private String genreName;

    public Genre(){

    }
    public String getGenreName() { //get method for genre name

        return genreName;
    }

    public Genre(String genreName) {
        this.genreName = genreName;
    } //Constructor contains genre name
}

