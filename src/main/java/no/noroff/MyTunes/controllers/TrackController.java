package no.noroff.MyTunes.controllers;

import no.noroff.MyTunes.dataAccess.TrackRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller //Controller class, used to return html pages
public class TrackController {
    private TrackRepository trackRepository = new TrackRepository(); //creating TrackRepository object, to use methods in TrackRepository class


    @GetMapping("/") //@GetMapping - handling incoming HTTP GET requests. Mapping "/" for method home
    public String home(Model model){ //takes a Model parameter, can be any form, eg. object or string etc.
        model.addAttribute("artists", trackRepository.getRandomArtists()); //adds array of random artists to model
        model.addAttribute("songs", trackRepository.getRandomSongs()); //adds array of random songs to model
        model.addAttribute("genres", trackRepository.getRandomGenres()); //adds array of random genres to model
        return "index"; //Spring boot looks for template index.html in templates folder.
    }

    @GetMapping("/search") //@GetMapping - handling incoming HTTP GET requests. Mapping "/search" for method getSearchTrack. Query string from form is added to uri
    public String getSearchTrack(@RequestParam(defaultValue = "") String songName, Model model){ //extracts form parameter songName, and takes a model as parameter
        model.addAttribute("searchTracks", new TrackRepository().getSearchTrack(songName)); //adds array of SearchTrack objects from method to model, with extracted form parameter songName
        return "search"; //Spring boot looks for template search.html in templates folder.
    }
}
