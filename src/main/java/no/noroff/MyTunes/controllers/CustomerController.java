package no.noroff.MyTunes.controllers;

import no.noroff.MyTunes.dataAccess.CustomerRepository;
import no.noroff.MyTunes.models.Country;
import no.noroff.MyTunes.models.Customer;
import no.noroff.MyTunes.models.Invoice;
import no.noroff.MyTunes.models.TopGenre;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/*
Every request handling method of the controller class automatically serializes objects into HttpResponse.
RestController is applied to a class to mark it as a request handler.
It looks through the entire application and list of endpoints, and looks for mappings in these classes. It takes the list
mappings and hands it over to the servlet, and decide based on incoming requests from the Postman, which controller to delegate to what request.
 */
@RestController

//create methods into endpoints by using requestMapping and sets the URL
@RequestMapping(value = "api/customers")
public class CustomerController {

    //to use the repository we create an object
    private CustomerRepository customerRepository = new CustomerRepository();

    //this Getmapping returns an array of customers and lists all the different customers in the database.
    //collects information from db.
    @GetMapping
    public ArrayList<Customer> getAllCustomers(){
        return customerRepository.getAllCustomers(); //collects data from CustomerRepository
    }
    //Postmapping to add a new Customer to the database.
    //Makes changes to the db.
    @PostMapping
    public Boolean addNewCustomer(@RequestBody Customer customer){
        return customerRepository.addCustomer(customer);
    }
    //Postmapping to update an existing customer in the db by their id.
    //Makes changes in the db.
    @PutMapping(value = "/{customerId}") //passes the id
    public Boolean updateExistingCustomer(@PathVariable int customerId, @RequestBody Customer customer){
        return customerRepository.updateCustomer(customer);
    }
    //Getmapping returns an array of country objects and lists the number of customers in different countries.
    @GetMapping("/byCountry")
    public ArrayList<Country> getCustomersInCountry(){
        return customerRepository.getNrCustomersInCountry();
    }

    //Getmapping returns an array of invoice objects and lists the customers that are the highest spenders in total.
    @GetMapping("/highestTotal")
    public ArrayList<Invoice> getCustomersHighestTotal(){
        return customerRepository.getCustomersHighestTotal();
    }

    //Getmapping returns an array of TopGenre objects and lists the customers must popular genre
    @RequestMapping(value = "/{customerId}/popular/genres", method = RequestMethod.GET)
    public ArrayList<TopGenre> getTopGenre(@PathVariable int customerId) {
        return customerRepository.getTopGenre(customerId);
    }




}

