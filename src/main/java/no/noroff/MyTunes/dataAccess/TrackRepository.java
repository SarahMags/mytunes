package no.noroff.MyTunes.dataAccess;

import no.noroff.MyTunes.models.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class TrackRepository {
    private String URL = ConnectionHelper.CONNECTION_URL; //Getting the database connection url
    private Connection conn = null; //Declaring a connection variable

    public ArrayList<Artist> getRandomArtists(){
        ArrayList<Artist> artists = new ArrayList<>(); //Empty array list of artist objects
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM Artist ORDER BY RANDOM() LIMIT 5"); //Randomizes names in Artist table, "Limit 5" returns the first 5 names
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery(); //Saves query result in a result set
            while (resultSet.next()) { //While there are more results from the query

                artists.add( //adds result to array list of artists
                        //Artist is a pojo that only contains a name variable
                        new Artist(
                                resultSet.getString("Name")  //Sets the name from the query into a new artist object
                        ));
            }
        }
        catch (Exception exception){
            System.out.println(exception); //Prints exception to the console if anything fails in try block
        }
        finally {
            try {
                conn.close(); //Closes connection to db
            }
            catch (Exception exception){
                System.out.println(exception);
            }
        }
        return artists; //Returns array of Artist objects, now containing the names from the query
    }


    public ArrayList<Song> getRandomSongs(){
        ArrayList<Song> songs = new ArrayList<>(); //Empty array list of song objects
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM Track ORDER BY RANDOM() LIMIT 5"); //Randomizes names in Track table, "Limit 5" returns the first 5 names
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();  //Saves query result in a result set
            while (resultSet.next()) { //While there are more results from the query

                songs.add( //adds result to array list of songs
                        //Song is a pojo that only contains a name variable
                        new Song(
                                resultSet.getString("Name") //Sets the name from the query into a new song object
                        ));
            }
        }
        catch (Exception exception){
            System.out.println(exception); //Prints exception to the console if anything fails in try block
        }
        finally {
            try {
                conn.close(); //Close connection to db
            }
            catch (Exception exception){
                System.out.println(exception);
            }
        }
        return songs; //Returns array of Song objects, now containing the names from the query
    }

    public ArrayList<Genre> getRandomGenres(){
        ArrayList<Genre> genres = new ArrayList<>(); //Empty array list of genre objects
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM Genre ORDER BY RANDOM() LIMIT 5"); //Randomizes names in Genre table, "Limit 5" returns the first 5 names
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery(); //Saves query result in a result set
            while (resultSet.next()) { //While there are more results from the query

                genres.add( //adds result to array list of genres
                        //Genre is a pojo that only contains a name variable
                        new Genre(
                                resultSet.getString("Name") //Sets the name from the query into a new song object
                        ));
            }
        }
        catch (Exception exception){
            System.out.println(exception); //Prints exception to the console if anything fails in try block
        }
        finally {
            try {
                conn.close(); //Close connection to db
            }
            catch (Exception exception){
                System.out.println(exception);
            }
        }
        return genres; //Returns array of Genre objects, now containing the names from the query
    }

    public ArrayList<SearchTrack> getSearchTrack(String songName){ //the songName parameter comes from the search field in index.html on submit
        ArrayList<SearchTrack> searchTracks = new ArrayList<>(); //Empty array list of SearchTrack objects
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            // Make SQL query
            //Selecting track, artist, album and genre, joining tables by common keys and matching the name in track with songName parameter
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Track.Name AS trackName, Artist.Name AS artistName,  " +
                            "Album.Title AS albumTitle, Genre.Name AS genreName from Genre " +
                            "Inner JOIN Track on Track.GenreId=Genre.GenreId " +
                            "INNER JOIN Album ON Track.AlbumId=Album.AlbumId " +
                            "INNER JOIN Artist ON Album.ArtistId=Artist.ArtistId WHERE LOWER(trackName) LIKE LOWER(?)"); //Using lower to ignore case, "LIKE" to match words that contain songName parameter
            preparedStatement.setString(1, "%" + songName + "%"); //Replacing first "?" in sql statement. The percentage symbols are added so the songName value can be found between any amount of other characters. (Word contains songName)
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery(); //Saves query result in a result set
            while (resultSet.next()) { //While there are more results from the query

                searchTracks.add( //adds result to array list of searchTracks
                        //SearchTrack objects contain name variables for song, artist, album and genre
                        new SearchTrack(
                                resultSet.getString("trackName"), //Song name
                                resultSet.getString("artistName"), //Artist
                                resultSet.getString("albumTitle"), // Album
                                resultSet.getString("genreName") //Genre
                        ));
            }
        }
        catch (Exception exception){
            System.out.println(exception); //Prints exception to the console if anything fails in try block
        }
        finally {
            try {
                conn.close(); //Close connection to db
            }
            catch (Exception exception){
                System.out.println(exception);
            }
        }
        return searchTracks; //Returns array of SearchTrack objects, now containing the names from the query
    }
}
