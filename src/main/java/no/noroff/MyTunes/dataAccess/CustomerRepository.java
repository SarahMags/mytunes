package no.noroff.MyTunes.dataAccess;

import no.noroff.MyTunes.models.*;

import java.sql.*;
import java.util.ArrayList;

public class CustomerRepository {
    //connection to db
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    //Method that list all customers by creating an array
    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customers = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();
            //iterate through the result and add customer to an array
            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"), //collects the data from the current row
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
        }
        catch (Exception exception){
            System.out.println(exception);
        } //close the connection to the db
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                System.out.println(exception);
            }
        } //return the customers back to the controller
        return customers;
    }

    //Method that adds a customer with a customer parameter
    public Boolean addCustomer(Customer customer){
        Boolean success = false; //set it to false
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) VALUES(?,?,?,?,?,?)");
            preparedStatement.setString(1,customer.getFirstName()); //define the first ? from the statement with a string and get the firstname
            preparedStatement.setString(2,customer.getLastName()); // define the second ? and get the lastname
            preparedStatement.setString(3,customer.getCountry());   //
            preparedStatement.setString(4,customer.getPostalCode());
            preparedStatement.setString(5,customer.getPhoneNr());
            preparedStatement.setString(6,customer.getEmail());
            // Execute Query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
        }
        catch (Exception exception){
            System.out.println(exception);
        }  //close the connection to the db
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                System.out.println(exception);
            }
        } //return true or false depending on if update is successful
        return success;
    }

    //This method is to update a customer customer paramter
    public Boolean updateCustomer(Customer customer){
        Boolean success = false;
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("UPDATE Customer SET FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? WHERE CustomerId = ?");
            preparedStatement.setString(1,customer.getFirstName()); //define the ? from the statement with a string and get the firstname
            preparedStatement.setString(2,customer.getLastName()); //same with the rest
            preparedStatement.setString(3,customer.getCountry());
            preparedStatement.setString(4,customer.getPostalCode());
            preparedStatement.setString(5,customer.getPhoneNr());
            preparedStatement.setString(6,customer.getEmail());
            preparedStatement.setInt(7,customer.getCustomerId()); //define the last ? from the statement with an int as it is a number
            // Execute Query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }//close the connection to the db
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }//return true or false depending on if update is successful
        return success;
    }

    // methods that lists number of customers in each country by creating an array
    public ArrayList<Country> getNrCustomersInCountry(){
        ArrayList<Country> countries = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT COUNT(CustomerId) AS NumberOfCustomers, Country FROM Customer GROUP BY Country ORDER BY NumberOfCustomers DESC;");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) { //if something was found in the resultSet
                countries.add(         //then it add the information to the array countries
                        new Country(
                                resultSet.getString("Country"),
                                resultSet.getInt("numberOfCustomers")
                        ));
            }
        }
        catch (Exception exception){
            System.out.println(exception);
        }//close the connection to the db
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                System.out.println(exception);
            }
        } //returns the array with the information from the SQL query
        return countries;
    }

    // This methods lists the number of highest spenders of the customers with an array
    public ArrayList<Invoice> getCustomersHighestTotal(){
        ArrayList<Invoice> invoices = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Invoice.CustomerId, Customer.FirstName, Customer.LastName, sum(Invoice.Total) as totalspent FROM Invoice INNER JOIN Customer ON Invoice.CustomerId=Customer.CustomerId GROUP BY Invoice.CustomerId order by totalspent desc");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) { //if something was found in the resultSet
                invoices.add(           // it is adds the information to the invoices list
                        new Invoice( //Sets the results from the query into a new Inovice object
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getDouble("totalSpent")
                        ));
            }
        }
        catch (Exception exception){
            System.out.println(exception);
        }//close the connection to the db
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                System.out.println(exception);
            }
        }
        return invoices;
    }

    public ArrayList<TopGenre> getTopGenre(int customerId){
        ArrayList<TopGenre> topGenres = new ArrayList<>(); //Empty arraylist of TopGenre objects
        int maxGenreNumber = 0; //Int variable to store the number corresponding to the customers most popular genre
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT COUNT(G.Name) AS Number\n" +
                            "FROM Customer\n" +
                            "JOIN Invoice I on Customer.CustomerId = I.CustomerId\n" +
                            "JOIN InvoiceLine IL on I.InvoiceId = IL.InvoiceId\n" +
                            "JOIN Track T on T.TrackId = IL.TrackId\n" +
                            "JOIN Genre G on G.GenreId = T.GenreId\n" +
                            "WHERE Customer.CustomerId = ?\n" +
                            "GROUP BY G.Name\n" +
                            "ORDER BY Number DESC"); //Selects and counts number of occurrences of the genre names for one specific customer, ordered by that number descending
            preparedStatement.setInt(1,customerId); //Replaces "?" with current customer id
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                maxGenreNumber=resultSet.getInt(1); //Stores the first row, which contains the largest number of occurrences of a genre, because the SQL is ordered descending


            }
            PreparedStatement preparedStatement2 = //Runs the same statement again, but now also selecting the genre name.
                    conn.prepareStatement("SELECT G.Name, COUNT(G.Name) AS Number\n" +
                            "FROM Customer\n" +
                            "JOIN Invoice I on Customer.CustomerId = I.CustomerId\n" +
                            "JOIN InvoiceLine IL on I.InvoiceId = IL.InvoiceId\n" +
                            "JOIN Track T on T.TrackId = IL.TrackId\n" +
                            "JOIN Genre G on G.GenreId = T.GenreId\n" +
                            "WHERE Customer.CustomerId = ?\n" +
                            "GROUP BY G.Name\n" +
                            "HAVING Number = ?"); //Using maxGenreNumber value to select genres in case of a tie. Selects all genres with the max value
            preparedStatement2.setInt(1,customerId); //Replaces the first "?"
            preparedStatement2.setInt(2,maxGenreNumber); //Replaces the second "?"
            // Execute Query
            ResultSet resultSet2 = preparedStatement2.executeQuery();

            while(resultSet2.next()){ //While there are more rows in result
                topGenres.add( //add new TopGenre objects to topGenres arraylist
                        new TopGenre(
                                resultSet2.getString("Name"), //Set values from query to new TopGenre object
                                resultSet2.getInt("Number")

                        ));

            }

        }
        catch (Exception exception){
            System.out.println(exception); //Print exception if anything in try block fails
        }
        finally {
            try {
                conn.close(); //Close connection to db
            }
            catch (Exception exception){
                System.out.println(exception);
            }
        }
        return topGenres; //Return arraylist of topGenre objects
    }


}
