## MyTunes

### Description
MyTunes is a working prototype composed of a simple UI built in Thymeleaf with the rest of the application being exposed as a regular REST API to be possibly consumed by a mobile app in the future. The web application was created with Spring Boot and Spring Initializr.

The project contains a database called Chinook, which emulates the iTunes functionality. It is a SQLite database for simplicity (SQLite JDBC driver).

MyTunes has two page views. One home page view, showing the 5 random artists, 5 random songs, and 5 random genres and a search bar which is used to search for tracks.
The second page is the search results page, showing the results from the requested track. The search finds all tracks that contain the searched value, and is also case insensitive. Results include track name, artist name, album and genre.

The Rest endpoints returns JSON data, and are on a /api/sub directory in the applications structure. These API endpoints caters to the following functionality:
*  Read all the customers in the database.
*  Add a new customer to the database.
*  Update an existing customer.
* Return the number of customers in each country, ordered descending (most number of customers to least)
*  Customers who are the highest spenders (total in invoice table is the largest), ordered descending.
* For a given customer, their most popular genre (in the case of a tie, display both).

The endpoints can be tested with the exported JSON file from postman in the project root folder.

The application is published as a Docker container on [Heroku](https://sarahmarenmytunes.herokuapp.com)
  
---  
Application made by:
Maren Ytterdal Krogsrud &
Sarah Thorstensen

---
Project repo can be found at: [GitLab](https://gitlab.com/SarahMags/mytunes)
